import os

from mapa_project.entity_detection.helpers.systematic_training import SystematicTrainingConfig


def test_systematic_training_config_yaml():
    example_yaml_path = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, 'example_files', 'systematic_train_config_example.yaml')

    config = SystematicTrainingConfig.load_config_from_yaml(yaml_config_path=example_yaml_path)

    print(config)

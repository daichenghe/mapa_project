from mapa_project.entity_detection.helpers.systematic_training import SystematicTrainingRunning

if __name__ == '__main__':
    # A quick check of this
    # base_config = '/DATA/agarciap_data/MAPA_STUFF/MAPA_FINAL/systematic_trainings/all_eurlex_trainings/all_eurlex_trainings_base_config.json'
    # specific_configs = '/DATA/agarciap_data/MAPA_STUFF/MAPA_FINAL/systematic_trainings/all_eurlex_trainings/' \
    #                    'all_eurlex_trainings_specific_configs.json'
    yaml_config = 'XXXXXXX'  # Add the corresponding yaml config (equivalent to the json, but way more neat)
    SystematicTrainingRunning(yaml_config_file=yaml_config).run_trainings(gpus=[1, 5, 6])

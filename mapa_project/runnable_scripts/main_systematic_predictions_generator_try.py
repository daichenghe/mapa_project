"""
A test script for the systematic predictions generation.
Based on configuration.
"""
from typing import Dict, Tuple, List

import confuse
from confuse import Configuration

from mapa_project.entity_detection.evaluation.custom_conll_gen_for_eval import ModelDescriptors
from mapa_project.entity_detection.evaluation.mapa_predictions_generator import MapaPredictionsGenerator


def main(config_file: str, cuda_device_num: int = -1):
    predictions_generation_config: Configuration = confuse.Configuration('MAPA_predictions_generation', __name__)
    predictions_generation_config.set_file(config_file)

    predictions_base_folder = predictions_generation_config['PREDICTIONS_BASE_FOLDER'].get(str)
    predictions_config: Dict = predictions_generation_config['PREDICTIONS_CONFIG'].get(dict)
    models_and_test_sets: List[Tuple[ModelDescriptors, str, str]] = []
    for config_label, config_values in predictions_config.items():
        model_path = config_values.pop('model_path')
        test_set_folder = config_values.pop('test_set_folder')
        model_descriptors = ModelDescriptors(**config_values)
        models_and_test_sets.append((model_descriptors, model_path, test_set_folder))

    MapaPredictionsGenerator.systematic_predictions_generation(models_and_test_sets=models_and_test_sets, base_output_folder=predictions_base_folder,
                                                               cuda_device_num=cuda_device_num)


if __name__ == '__main__':
    config_file = '/DATA/agarciap_data/MAPA_STUFF/MAPA_FINAL/systematic_trainings/trainings_for_eval/predictions_generation_config_for_eval.yaml'
    cuda_device_num = 6
    main(config_file=config_file, cuda_device_num=cuda_device_num)

import json
from dataclasses import dataclass
from typing import List, Tuple

# from mapa_project.anonymization.data_formats import MapaResult
from mapa_project.anonymization.datatypes_definition import MapaResult
from mapa_project.data_processing.mapa_hierarchy import MAPA_HIERARCHY

TypeAndColor = Tuple[str, str]


@dataclass
class BratEntityType:
    type: str
    labels: List[str]
    bgColor: str = '#7fa2ff'
    borderColor: str = 'darken'


@dataclass
class BratFormatting:
    entity_types: List[BratEntityType]


@dataclass
class BratEntity:
    id: str
    type: str
    spans: List[Tuple[int, int]]


@dataclass
class BratDocument:
    text: str
    entities: List[Tuple[str, str, Tuple[Tuple[int, int]]]]

    def __init__(self, text: str, entities: List[BratEntity]):
        self.text = text
        # self.entities = [(e.id, e.type, ((*e.spans),)) for e in entities] # python incompatibility with the starred expression? (when Python3.9)
        self.entities = [(e.id, e.type, tuple((span for span in e.spans))) for e in entities]


class BratFormatProcessor:
    """
    Format example:
    {
      "text":"this is an example",
      "entities":[["T1","Person",[[5,7]]]]
    }
    """

    def __init__(self, entity_types: List[BratEntityType], mapa_result: MapaResult, obfuscate_entities: bool = False):
        self.brat_formatting: BratFormatting = BratFormatting(entity_types=entity_types)
        self.obfuscate_entities: bool = obfuscate_entities
        self.brat_document: BratDocument = self.__convert_to_brat_format(text=mapa_result.text, mapa_result=mapa_result)

    def convert_entities_formatting_to_json(self) -> str:
        return json.dumps(self.brat_formatting)

    def convert_brat_document_to_json(self) -> str:
        return json.dumps(self.brat_document)

    def __convert_to_brat_format(self, text: str, mapa_result: MapaResult) -> BratDocument:

        level1_merged = [(entity.entity_type, (entity.offset, entity.offset + len(entity.entity_text))) for entity in mapa_result.level1_entities]
        level2_merged = [(entity.entity_type, (entity.offset, entity.offset + len(entity.entity_text))) for entity in mapa_result.level2_entities]

        text_chars = list(text)
        brat_entities: List[BratEntity] = []
        for entity_type, (start, end) in level1_merged:
            if self.__is_empty(text, start, end):
                continue
            brat_entity = BratEntity(id=f'T{len(brat_entities) + 1}', type=entity_type, spans=[(start, end)])
            brat_entities.append(brat_entity)
            if self.obfuscate_entities:
                text_chars[start:end] = '*' * (end - start)

        for entity_type, (start, end) in level2_merged:
            if self.__is_empty(text, start, end):
                continue
            brat_entity = BratEntity(id=f'T{len(brat_entities) + 1}', type=entity_type, spans=[(start, end)])
            brat_entities.append(brat_entity)

        brat_doc = BratDocument(text=''.join(text_chars), entities=brat_entities)
        return brat_doc

    @classmethod
    def __is_empty(cls, text, start, end):
        return text[start:end].strip() == ''

# taken from: https://colorbrewer2.org/#type=qualitative&scheme=Paired&n=12
colors = ['#a6cee3', '#1f78b4', '#b2af8a', '#33a02c', '#ab9a59', '#e31a1c', '#fdbf6f', '#aa5d00', '#cab2d6', '#6a3d9a', '#aaaa99', '#b15928']

# taken from https://coolors.co/
level1_colors = ["33a8c7", "52e3e1", "a0e426", "fdf148", "ffab00", "f77976", "f050ae", "d883ff", "9336fd"]
level2_colors = ["f94144", "f3722c", "f8961e", "f9844a", "f9c74f", "90be6d", "43aa8b", "4d908e", "577590", "277da1"]

level1_colors.reverse()
level2_colors.reverse()


def get_labels_hierarchy():
    # I am encapsulating it in a function in case it need any further post-processing, but it should not be necessary
    hierarchy = MAPA_HIERARCHY
    return hierarchy


def generate_brat_entity_types() -> List[BratEntityType]:
    # default_color = '#999999'
    hierarchy = get_labels_hierarchy()
    level1_types_and_colors = []
    level2_types_and_colors = []
    for i, (level1_tag, level2_tags) in enumerate(sorted(hierarchy.items())):
        level1_color = level1_colors[i % len(level1_colors)]
        level1_types_and_colors.append((level1_tag, '#' + level1_color))
        for j, level2_tag in enumerate(level2_tags):
            level2_color = level2_colors[j % len(level2_colors)]
            level2_types_and_colors.append((level2_tag, '#' + level2_color))

    brat_entity_types: List[BratEntityType] = []
    for label_type, color in sorted(level1_types_and_colors, key=lambda x: x[0]) + sorted(level2_types_and_colors, key=lambda x: x[0]):
        brat_entity_types.append(BratEntityType(type=label_type, labels=[label_type, label_type[:3]], bgColor=color))

    return brat_entity_types

from dataclasses import dataclass
from enum import auto, Enum
from typing import Optional, List

from pydantic import BaseModel

# from mapa_project.anonymization.data_formats import MapaEntity


# from mapa_project.webservice.config import AutoName
from mapa_project.anonymization.datatypes_definition import MapaEntity


class AutoName(Enum):
    def _generate_next_value_(self, start, count, last_values):
        # self is the name here
        return self


@dataclass
class AnalysisInfo:
    num_tokens: int
    elapsed_time: float
    used_device: str


class AnonymizationOp(AutoName):
    OBFUSCATE = auto()
    REPLACE = auto()
    TYPE_SUBSTITUTION = auto()
    NOOP = auto()


class AnonymizationRequest(BaseModel):
    text: str
    model_label: Optional[str]
    operation: Optional[AnonymizationOp] = AnonymizationOp.OBFUSCATE


class Annotation(BaseModel):
    content: str
    begin: int
    end: int
    value: str

    @classmethod
    def from_mapa_entity(cls, mapa_entity: MapaEntity):
        return Annotation(content=mapa_entity.entity_text, begin=mapa_entity.offset, end=mapa_entity.offset + len(mapa_entity.entity_text),
                          value=mapa_entity.entity_type)


class AnonymizationResponse(BaseModel):
    anonymized_text: str
    annotations: List[Annotation]
    elapsed_seconds: float
    words_per_second: float
    used_device: str


class DeployedModelsInfo(BaseModel):
    model_label: str
    is_default: bool

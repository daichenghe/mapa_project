import logging
from argparse import ArgumentParser

import os
import uvicorn
from fastapi import FastAPI
from starlette.staticfiles import StaticFiles

from mapa_project.anonymization.components_loading_and_managing import LoadedComponentsManager
from mapa_project.webservice.app.webservice_controller import STATIC_FILES_PATH, mapa_web_service_router
from mapa_project.webservice.loaded_components_managing import load_or_get_components_manager
from mapa_project.webservice.proxy_header_middleware import ProxyHeadersMiddleware

logger = logging.getLogger(__name__)

APP_PREFIX = '/mapa/1.0'


def create_general_app():
    argparse = ArgumentParser('MAPA web service launch', add_help=True)
    argparse.add_argument('--mapa_port', type=int, required=False, help='The port on which the service will listen')
    argparse.add_argument('--mapa_base_folder', type=str, required=False,
                          help=f'The base folder to look for the models and other configuration assets')

    # The arguments (port and base folder) are optional, if present, they override the env vars, if not, env vars will be used
    args = argparse.parse_args()
    if args.mapa_port:
        os.environ[LoadedComponentsManager.MAPA_PORT_ENV_VAR] = str(args.mapa_port)  # we need it to be a str to inject in the env vars
    if args.mapa_base_folder:
        os.environ[LoadedComponentsManager.MAPA_BASE_FOLDER_ENV_VAR] = args.mapa_base_folder

    logger.info('STARTING MAPA >> ENTITY-ANNOTATION << SERVICE')
    load_or_get_components_manager()
    app = FastAPI()
    app2 = FastAPI(root_path=APP_PREFIX,
                   title="MAPA deep-learning based anonymization platform (BETA)",
                   description="API for the MAPA anonymization platform services (BETA)",
                   version="1.0"
                   )

    app.mount(APP_PREFIX, app2)
    app2.include_router(mapa_web_service_router, prefix="/anonymization", tags=["Anonymization"])
    app2.mount(f"{APP_PREFIX}/static", StaticFiles(directory=STATIC_FILES_PATH), name="static")
    app.add_middleware(ProxyHeadersMiddleware, trusted_hosts="*")
    return app


_app = create_general_app()


def main():
    uvicorn.run('mapa_project.webservice.main:_app', host="0.0.0.0", port=load_or_get_components_manager().mapa_port,
                log_level="info", proxy_headers=True)


if __name__ == "__main__":
    main()

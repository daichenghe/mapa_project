"""
Evaluation (more precisely validation) metrics and logic to measure the progress of the model during training (againts a dev-set)
Not to be confused with more curated and specific evaluation logic for evaluation using a test-set once the training is over.
"""
import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import List, Tuple, Optional, ClassVar, Dict, Union

import torch
from sklearn.metrics import f1_score, recall_score, precision_score
from torch import Tensor

from mapa_project.entity_detection.common.field_definition import FieldDefinition


class BaseModelOutcomePostProcessor(ABC):

    @abstractmethod
    def process(self, gold_labels: Tensor, model_outcomes: Tensor) -> Tuple[Tensor, Tensor]:
        """ Receives the gold labels and model outcomes for a given field, and returns the post-processed versions of gold and outcomes """
        pass


class BaseEvaluationMetric(ABC):

    def __init__(self, name: str, target_field: Optional[FieldDefinition], preprocessor: Optional[BaseModelOutcomePostProcessor]):
        self.name = name
        self.target_field: FieldDefinition = target_field
        self.preprocessor = preprocessor

    def evaluate(self, gold_labels: Tensor = None, model_outcomes: Tensor = None, loss: Tensor = None):
        if self.preprocessor:
            gold_labels, model_outcomes = self.preprocessor.process(gold_labels=gold_labels, model_outcomes=model_outcomes)
        return self.calculate_metric_value(gold_labels=gold_labels, model_outcomes=model_outcomes, loss=loss)

    @abstractmethod
    def calculate_metric_value(self, gold_labels: Tensor = None, model_outcomes: Tensor = None, loss: Tensor = None):
        """
        A generic evaluation method that receives gold labels and model outcomes at calculates a metric value.
        The inputs can be anything, as long as the implemented logic knows how to deal with them.
        :param gold_labels: whichever number and type of gold values
        :param model_outcomes: whichever number and type of model outcomes (logits, predicted labels, etc.)
        :param loss: only used to directly pass the loss (in this case other parameters can be None)
        :return: the calculated value of the metric
        """
        pass


class EvaluationMetricCombination:

    def __init__(self, name: str, metrics_to_combine: Tuple[BaseEvaluationMetric, ...], weights: Optional[Tuple[float, ...]] = None):
        if weights and len(metrics_to_combine) != len(weights):
            raise Exception(f'Weights for metric combination ({name}) have been defined but do not match in number: '
                            f'{len(weights)} for {len(metrics_to_combine)} metrics')

        self.name = name
        self.metrics_to_combine = metrics_to_combine
        self.weights = weights

    def combine(self, metric_names_and_values: Dict[str, float]):
        # by default, average the metrics (weighting them or not)
        # there could be some other things, like harmonic averaging (similar to F-scoring) but... can done later (or specialise the class)
        values_to_combine = [metric_names_and_values[metric.name] for metric in self.metrics_to_combine]
        if self.weights:
            values_to_combine = [value * self.weights[i] for i, value in enumerate(values_to_combine)]
        averaged_values = sum(values_to_combine) / len(values_to_combine)
        return averaged_values


class GenericLossMetric(BaseEvaluationMetric):

    def calculate_metric_value(self, gold_labels: Tensor = None, model_outcomes: Tensor = None, loss: Tensor = None):
        """Loss simply forwards the received loss value"""
        return loss


class BaseEvaluationMetricsAccumulator(ABC):

    @abstractmethod
    def calculate_and_accumulate_metrics(self, gold_labels, model_outcomes, loss) -> List[Tuple[str, float]]:
        """
        Send the values to each metric calculation, to accumulate it. Loss is directly accumulated.
        Note: I have been thinking about making it possible to have more than a single loss
        But it complicates the handling of the loss during training step (loss.mean, gradients accumulation).
        Since such a thing is far from being critical or even necessary, for now, I discard it.
        :param gold_labels: gold values that the instantiated evaluation metrics will understand
        :param model_outcomes: model outcomes (predictions, logits, etc.) that the metrics will understand
        :param loss: the loss value
        :return:
        """
        pass

    @abstractmethod
    def reset_metric_accumulation(self, metric_name):
        pass

    @abstractmethod
    def get_averaged_metric_value(self, metric_name):
        pass

    @abstractmethod
    def get_all_metrics_average(self, reset_afterwards=False):
        pass

    @abstractmethod
    def reset_all_metrics_accumulation(self):
        pass


class SimpleEvaluationMetricsAccumulator(BaseEvaluationMetricsAccumulator):
    """
    An object that accumulates the values, and at some point averages the values.
    """
    logger: ClassVar[logging.Logger] = logging.getLogger(__name__)

    def __init__(self, evaluation_metrics: List[Union[BaseEvaluationMetric, EvaluationMetricCombination]]):
        self.evaluation_metrics: List[BaseEvaluationMetric] = [ev for ev in evaluation_metrics if isinstance(ev, BaseEvaluationMetric)]
        self.metric_combinations: List[EvaluationMetricCombination] = [ev for ev in evaluation_metrics if isinstance(ev, EvaluationMetricCombination)]
        # check that all the metrics in the combinations are added to the evaluation metrics so they can be computed
        self.__check_and_add_combination_metrics()
        self.accumulated_metric_values = {metric.name: 0 for metric in evaluation_metrics}
        self.accumulated_metric_counts = {metric.name: 0 for metric in evaluation_metrics}
        self.logger.info(f'Evaluation metrics configured in the metrics accumulator:{[m.name for m in self.evaluation_metrics]}')

    def __check_and_add_combination_metrics(self):
        for metric_combination in self.metric_combinations:
            for metric in metric_combination.metrics_to_combine:
                if metric not in self.evaluation_metrics:
                    self.evaluation_metrics.append(metric)

    def __accumulate_metric(self, metric_name, metric_value):
        if metric_name in self.accumulated_metric_values:
            self.accumulated_metric_values[metric_name] += metric_value
            self.accumulated_metric_counts[metric_name] += 1
        else:
            # since the initialisation with names this should be no longer possible, but it does not do any harm
            self.accumulated_metric_values[metric_name] = metric_value
            self.accumulated_metric_counts[metric_name] = 1

    def calculate_and_accumulate_metrics(self, gold_labels, model_outcomes, loss) -> List[Tuple[str, float]]:
        calculated_metric_values = []
        for evaluation_metric in self.evaluation_metrics:
            if evaluation_metric.target_field:
                target_logits = model_outcomes[evaluation_metric.target_field.name]
                target_labels = gold_labels[evaluation_metric.target_field.name]
                metric_value = evaluation_metric.evaluate(target_labels, target_logits, loss)
            else:
                metric_value = evaluation_metric.evaluate(gold_labels, model_outcomes, loss)
            self.__accumulate_metric(evaluation_metric.name, metric_value)
            calculated_metric_values.append((evaluation_metric.name, metric_value))
        # use the already calculated individual values for the combinations
        calculated_metric_values_as_dict = {name: value for name, value in calculated_metric_values}
        for metric_combination in self.metric_combinations:
            value = metric_combination.combine({m.name: calculated_metric_values_as_dict[m.name] for m in metric_combination.metrics_to_combine})
            self.__accumulate_metric(metric_combination.name, value)
            calculated_metric_values.append((metric_combination.name, value))
        return calculated_metric_values

    def reset_metric_accumulation(self, metric_name):
        self.accumulated_metric_values[metric_name] = 0
        self.accumulated_metric_counts[metric_name] = 0

    def get_averaged_metric_value(self, metric_name):
        if metric_name in self.accumulated_metric_counts and self.accumulated_metric_counts[metric_name] > 0:
            return self.accumulated_metric_values[metric_name] / self.accumulated_metric_counts[metric_name]
        else:
            return 0.0

    def get_all_metrics_average(self, reset_afterwards=False):
        all_metrics_average = {}
        for metric_name in self.accumulated_metric_values.keys():
            all_metrics_average[metric_name] = self.get_averaged_metric_value(metric_name)
        if reset_afterwards:
            self.reset_all_metrics_accumulation()
        return all_metrics_average

    def reset_all_metrics_accumulation(self):
        for metric_name in self.accumulated_metric_values.keys():
            self.reset_metric_accumulation(metric_name)


@dataclass
class EvaluationMetricsHolder:
    """
    A commodity class to group useful information about the evaluation metrics.
     - evaluation_metrics: The evaluation metrics themselves (or Evaluation Metrics Combination object).
     - metric_to_check: Which evaluation metric to check for checkpoint saving and early stopping
     - metrics_in_progress_bar: Which metrics to print in the progress bar (to avoid cluttering it too much...)
     - metrics_in_checkpoint_name: Which metrics use in the checkpoint folder name (to control its length)
     - maximize_checked_metric: if the checked metric is considered better when higher or the opposite
    """
    logger: ClassVar[logging.Logger] = logging.getLogger(__name__)

    evaluation_metrics: List[Union[BaseEvaluationMetric, EvaluationMetricCombination]]
    metric_to_check: Union[BaseEvaluationMetric, EvaluationMetricCombination]
    maximize_checked_metric: bool
    metrics_in_progress_bar: List[Union[BaseEvaluationMetric, EvaluationMetricCombination]]
    metrics_in_checkpoint_name: List[Union[BaseEvaluationMetric, EvaluationMetricCombination]]

    # Added explicitly to fix the problem with Cython and dataclasses (that eventually will get solved)
    # See: https://stackoverflow.com/questions/56079419/using-dataclasses-with-cython
    __annotations__ = {
        'evaluation_metrics': List[Union[BaseEvaluationMetric, EvaluationMetricCombination]],
        'metric_to_check': Union[BaseEvaluationMetric, EvaluationMetricCombination],
        'maximize_checked_metric': bool,
        'metrics_in_progress_bar': List[Union[BaseEvaluationMetric, EvaluationMetricCombination]],
        'metrics_in_checkpoint_name': List[Union[BaseEvaluationMetric, EvaluationMetricCombination]]
    }

    def __post_init__(self):
        self.__add_default_loss()
        self.logger.info(f'Evaluation metrics: {[metric.name for metric in self.evaluation_metrics]}')
        self.logger.info(f'Evaluation metric to {"MAXIMIZE" if self.maximize_checked_metric else "MINIMIZE"} '
                         f'for checkpoint-saving/early-stopping: {self.metric_to_check.name}')

    def __add_default_loss(self):
        # the target_field_name=None only applies to the loss because it is not part of the logits/labels
        loss_metric = GenericLossMetric(name='loss', target_field=None, preprocessor=None)
        self.evaluation_metrics.append(loss_metric)
        self.metrics_in_progress_bar.append(loss_metric)
        self.metrics_in_checkpoint_name.append(loss_metric)


###################
# Specific evaluation metrics (common ones like Precision/Recall/F-score)


class PrecisionMetric(BaseEvaluationMetric):

    def __init__(self, name: str, target_field: Optional[FieldDefinition], preprocessor: Optional[BaseModelOutcomePostProcessor], average_method: str,
                 labels_to_evaluate: Optional[List[int]] = None, binarization_negative_labels: Optional[List[int]] = None):
        super().__init__(name, target_field, preprocessor)
        self.labels_to_evaluate = labels_to_evaluate
        self.binarization_negative_labels = binarization_negative_labels
        if self.labels_to_evaluate and self.binarization_negative_labels:
            raise Exception(f'labels_to_evaluate and binarization_negative_labels cannot be set at the same time')
        self.average_method = average_method

    def calculate_metric_value(self, gold_labels: Tensor = None, model_outcomes: Tensor = None, loss: Tensor = None):
        if self.binarization_negative_labels:
            bin_gold_labels, bin_model_outcomes = binarize_evaluation_values(gold_labels=gold_labels, model_outcomes=model_outcomes,
                                                                             negative_binarization_labels=self.binarization_negative_labels)
            return precision_score(numpy_friendly(bin_gold_labels), numpy_friendly(bin_model_outcomes), pos_label=1, average='binary')
        else:
            return precision_score(numpy_friendly(gold_labels), numpy_friendly(model_outcomes), labels=self.labels_to_evaluate,
                                   average=self.average_method)


class RecallMetric(BaseEvaluationMetric):

    def __init__(self, name: str, target_field: Optional[FieldDefinition], preprocessor: Optional[BaseModelOutcomePostProcessor], average_method: str,
                 labels_to_evaluate: Optional[List[int]] = None, binarization_negative_labels: Optional[List[int]] = None):
        super().__init__(name, target_field, preprocessor)
        self.labels_to_evaluate = labels_to_evaluate
        self.binarization_negative_labels = binarization_negative_labels
        if self.labels_to_evaluate and self.binarization_negative_labels:
            raise Exception(f'labels_to_evaluate and binarization_negative_labels cannot be set at the same time')
        self.average_method = average_method

    def calculate_metric_value(self, gold_labels: Tensor = None, model_outcomes: Tensor = None, loss: Tensor = None):
        if self.binarization_negative_labels:
            bin_gold_labels, bin_model_outcomes = binarize_evaluation_values(gold_labels=gold_labels, model_outcomes=model_outcomes,
                                                                             negative_binarization_labels=self.binarization_negative_labels)
            return recall_score(numpy_friendly(bin_gold_labels), numpy_friendly(bin_model_outcomes), pos_label=1, average='binary')
        else:
            return recall_score(numpy_friendly(gold_labels), numpy_friendly(model_outcomes), labels=self.labels_to_evaluate,
                                average=self.average_method)


class FscoreMetric(BaseEvaluationMetric):

    def __init__(self, name: str, target_field: Optional[FieldDefinition], preprocessor: Optional[BaseModelOutcomePostProcessor], average_method: str,
                 labels_to_evaluate: Optional[List[int]] = None, binarization_negative_labels: Optional[List[int]] = None,
                 multilabel_threshold: Optional[float] = None):
        super().__init__(name, target_field, preprocessor)
        self.labels_to_evaluate = labels_to_evaluate
        self.binarization_negative_labels = binarization_negative_labels
        if self.labels_to_evaluate and self.binarization_negative_labels:
            raise Exception(f'labels_to_evaluate and binarization_negative_labels cannot be set at the same time')
        self.average_method = average_method
        self.multilabel_threshold = multilabel_threshold
        if self.binarization_negative_labels and self.multilabel_threshold:
            raise Exception(f'multilabel_threshold and binarization_negative_labels cannot be set at the same time')

    def calculate_metric_value(self, gold_labels: Tensor = None, model_outcomes: Tensor = None, loss: Tensor = None):
        if self.binarization_negative_labels:
            bin_gold_labels, bin_model_outcomes = binarize_evaluation_values(gold_labels=gold_labels, model_outcomes=model_outcomes,
                                                                             negative_binarization_labels=self.binarization_negative_labels)
            return f1_score(numpy_friendly(bin_gold_labels), numpy_friendly(bin_model_outcomes), pos_label=1, average='binary')
        else:
            if self.multilabel_threshold:
                model_outcomes = model_outcomes.ge(self.multilabel_threshold).long()
            return f1_score(numpy_friendly(gold_labels), numpy_friendly(model_outcomes), labels=self.labels_to_evaluate, average=self.average_method)


def numpy_friendly(t: Tensor):
    """ Ensure that a Pytorch tensor can be converted into a numpy tensor (for sklearn functions mainly) """
    return t.detach().cpu()


def binarize_evaluation_values(gold_labels: Tensor, model_outcomes: Tensor, negative_binarization_labels: List[int]):
    bin_gold_labels = torch.tensor([label not in negative_binarization_labels for label in gold_labels.view(-1)]).view(gold_labels.shape)
    bin_model_outcomes = torch.tensor([label not in negative_binarization_labels for label in model_outcomes.view(-1)]).view(model_outcomes.shape)
    return bin_gold_labels, bin_model_outcomes


class SequenceLabellingPostProcessor(BaseModelOutcomePostProcessor):

    def __init__(self, pad_idx: int, ctx_len: int, num_start_special_tokens: int = 1, num_end_special_tokens: int = 1):
        self.pad_idx: int = pad_idx
        self.start_ctx: int = (ctx_len or 0) + num_start_special_tokens
        self.end_ctx: int = (ctx_len or 0) + num_end_special_tokens
        self.total_ctx_len: int = self.start_ctx + self.end_ctx
        # to account for the CSL/SEP or whichever other special tokens systematically added to each sequence
        self.num_start_special_tokens = num_start_special_tokens
        self.num_end_special_tokens = num_end_special_tokens

    def process(self, gold_labels: Tensor, model_outcomes: Tensor) -> Tuple[Tensor, Tensor]:
        """
        Removes the disposable context and the padding positions, and also interprets the outcomes (argmax for multiclass, sigmoid for multilabel)
        :param gold_labels: gold labels, expects a shape of BxS (for multiclass) and BxSxN (for multilabel)
        :param model_outcomes: model logits, expects a shape of BxSxN both for multiclass and multilabel
        :return: a tuple with gold labels and model scores, of shape S' (multiclass) or S'xN (multilabel) (S' = BxS - removed positions)
        """
        golds: Tensor = gold_labels
        logits = model_outcomes
        multilabel_setting = len(gold_labels.shape) == 3
        # depending on multi/single label setting, do something different with the logits (max, or sigmoid)
        outcomes = torch.sigmoid(logits) if multilabel_setting else torch.max(logits, dim=2)[1]

        # Using torch.narrow which seems to do exactly this: https://medium.com/@tankalasrkr/5-useful-tensor-operations-in-pytorch-6be49e69977d
        golds = torch.narrow(golds, dim=1, start=self.start_ctx, length=golds.shape[1] - self.total_ctx_len)
        outcomes = torch.narrow(outcomes, dim=1, start=self.start_ctx, length=outcomes.shape[1] - self.total_ctx_len)

        if multilabel_setting:
            # first change the view to (BxS')xN of both gold and pred
            golds = golds.reshape(-1, golds.shape[2])
            outcomes = outcomes.reshape(-1, outcomes.shape[2])
            # mask could be taken from the "pad_idx" position of each entry
            # invert the padding markers, so it truly becomes 0 when padding and 1 when non-padding
            non_padding_mask = golds[:, self.pad_idx].reshape(-1).ne(1.)
            # now careful, a bit tricky/dangerous operation (expand mask, so the masked select picks N elements for each active position) then reshape
            non_padding_mask = non_padding_mask.unsqueeze(1).expand(-1, golds.shape[1])  # shape[1] because we have already changed the view
            golds = torch.masked_select(golds, mask=non_padding_mask).reshape(-1, golds.shape[1])
            outcomes = torch.masked_select(outcomes, mask=non_padding_mask).reshape(-1, outcomes.shape[1])
        else:
            # create a flat mask to select all the non-padding (gold) positions
            non_padding_mask = golds.reshape(-1).ne(self.pad_idx)
            # apply the mask to select the gold non-padding positions both from golds and preds
            golds = torch.masked_select(golds.reshape(-1), mask=non_padding_mask)
            outcomes = torch.masked_select(outcomes.reshape(-1), mask=non_padding_mask)
        return golds, outcomes

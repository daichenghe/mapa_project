"""
A simple module to match preconfigured expressions.
"""
from itertools import chain
from re import Pattern
from typing import List, Tuple, Dict, FrozenSet

import re


class PatternMatcher:

    def __init__(self, named_patterns: List[Tuple[str, str]]):
        r"""
        Initializes the PatternMatcher with the configured patterns.
        Example: [('DNI', '\d{8}[A-Z]')]
        :param named_patterns: tuple of pattern name and pattern regexp
        """
        self.raw_patterns = {name: pattern for name, pattern in named_patterns}

        # self.compiled_patterns = {name: re.compile(pattern) for name, pattern in named_patterns}
        self.compiled_patterns: Dict[str, Pattern] = {}
        for name, raw_pattern in self.raw_patterns.items():
            # print(f'Compiling {name} -> {raw_pattern}')
            self.compiled_patterns[name] = re.compile(raw_pattern)

    @classmethod
    def load_from_files(cls, paths: List[str]):
        named_patterns = []
        for path in paths:
            named_patterns += cls.parse_named_patterns_file(path)
        return PatternMatcher(named_patterns=named_patterns)

    @classmethod
    def parse_named_patterns_file(cls, path) -> List[Tuple[str, str]]:
        """ Loads the named patterns from a file, each line must be NAME::REGEX, empty lines and the ones starting with # are ignored """
        with open(path, 'r', encoding='utf-8') as f:
            lines = f.readlines()
        # empty lines and lines starting with # are omitted
        filtered_lines = [line.strip() for line in lines if line.strip() != '' and not line.strip().startswith('#')]
        names_and_patterns = [line.split('::', 2) for line in filtered_lines]
        names_and_patterns = [(name_and_pattern[0], name_and_pattern[1]) for name_and_pattern in names_and_patterns]
        return names_and_patterns

    def find_patterns(self, text: str, drop_overlapping: bool = True) -> Dict[str, List[Tuple[int, int, str]]]:
        """
        Return each character span that matches a pattern, together with its assigned type
        :param text: the text to perform the match against
        :param drop_overlapping: a flag indicating when the overlapping matches should be dropped, preferring the longest ones
        :return:
        """
        named_matches: Dict[str, List[Tuple[int, int, str]]] = {}
        for name, compiled_pattern in self.compiled_patterns.items():
            matched_spans: List[Tuple[int, int, str]] = []
            compiled_pattern: Pattern
            founds = compiled_pattern.finditer(text)
            for found in founds:
                matched_spans.append((found.start(), found.end(), text[found.start():found.end()]))

            if len(matched_spans) > 0:
                named_matches[name] = matched_spans

        if drop_overlapping:
            # note: in-place removal
            self.__drop_overlapping(named_matches=named_matches)
        # print('PATTERNS MATCHED', named_matches)
        return named_matches

    @classmethod
    def __drop_overlapping(cls, named_matches: Dict[str, List[Tuple[int, int, str]]]):
        """ For each overlapping span, keep only the longest """
        all_spans = list(chain(*named_matches.values()))
        # print('all_spans', all_spans)
        spans_to_be_dropped = set()
        for span1 in all_spans:
            for span2 in all_spans:
                if span1 == span2:
                    continue
                span1_char_set = set(range(span1[0], span1[1]))
                span2_char_set = set(range(span2[0], span2[1]))
                # print(f'{span1} and {span2} disjoint -> {span1_char_set.isdisjoint(span2_char_set)}')
                if not span1_char_set.isdisjoint(span2_char_set):
                    if len(span2_char_set) > len(span1_char_set):
                        spans_to_be_dropped.add(span1)
                    elif len(span2_char_set) < len(span1_char_set):
                        spans_to_be_dropped.add(span2)

        # print('spans to be dropped:', spans_to_be_dropped)
        for name in list(named_matches.keys()):
            matched_spans = named_matches[name]
            for span in matched_spans.copy():
                # print(f'{name} -> {span} in {spans_to_be_dropped} == {span in spans_to_be_dropped}')
                if span in spans_to_be_dropped:
                    # print(f'{name} -> matched spans before', matched_spans)
                    matched_spans.remove(span)
                    # print(f'{name} -> matched spans after', matched_spans)
        # drop keys with empty lists
        empty_keys = [key for key, val in named_matches.items() if len(val) == 0]
        for empty_key in empty_keys:
            named_matches.pop(empty_key)
        # it is doing the removal in-place, no need of returning any value

    def match_entities(self, tokens: List[str], spans: List[Tuple[int, int]], labels: List[str]):
        rebuilt_text = self.__rebuild_text(tokens, spans)
        named_matches = self.find_patterns(rebuilt_text)
        labels_from_patterns = self.__determine_tokens(tokens=tokens, token_spans=spans, named_matches=named_matches)
        labels_with_patterns = [labels_from_patterns[i] if label == 'O' else label for i, label in enumerate(labels)]
        bio_tagged_labels_with_patterns = self.__add_bio_tagging_to_pattern_labels(labels_with_patterns)
        return bio_tagged_labels_with_patterns

    @classmethod
    def __add_bio_tagging_to_pattern_labels(cls, labels: List[str]):
        bio_tagged_labels = labels.copy()
        for i, label in enumerate(labels):
            if label == 'O':
                continue
            elif '-' in label:
                continue
            else:
                # we are in a non-O label that has no BIO-tagging so far
                if i == 0:
                    # cannot be but a B, since it is the very first label of the sequence
                    bio_tagged_labels[i] = f'B-{label}'
                elif labels[i - 1] == label:
                    # previous label was exactly the same, so we are inside the same entity (it is a rough assumption, but should suffice)
                    bio_tagged_labels[i] = f'I-{label}'
                else:
                    # the only remaining case, starting a new type of entity
                    bio_tagged_labels[i] = f'B-{label}'
        return bio_tagged_labels

    @classmethod
    def __determine_tokens(cls, tokens: List[str],
                           token_spans: List[Tuple[int, int]], named_matches: Dict[str, List[Tuple[int, int, str]]]) -> List[str]:

        # revert the named matches to obtain a dict of matched_span -> name  (omg... check that this one-liner is correct...)
        reserved_named_matches: Dict[FrozenSet, str] = {k: v for d in
                                                        [{frozenset(range(span[0], span[1])): name for span in spans} for name, spans in
                                                         named_matches.items()] for k, v in
                                                        d.items()}

        token_covered_chars_sets = [frozenset([i for i in range(start, end)]) for start, end in token_spans]
        pattern_covered_chars_sets = [k for k in reserved_named_matches.keys()]

        labels_from_patterns = []
        for i, token_covered_chars_set in enumerate(token_covered_chars_sets):
            matched_by_pattern = False
            for pattern_covered_chars_set in pattern_covered_chars_sets:
                if token_covered_chars_set.intersection(pattern_covered_chars_set) and tokens[i]:
                    labels_from_patterns.append(reserved_named_matches[pattern_covered_chars_set])
                    matched_by_pattern = True
                    break
            if not matched_by_pattern:
                labels_from_patterns.append('O')
        return labels_from_patterns

    @classmethod
    def __rebuild_text(cls, tokens: List[str], spans: List[Tuple[int, int]]):
        text = []
        last_offset = 0
        for i, token in enumerate(tokens):
            start, end = spans[i]
            text.append(' ' * (start - last_offset))
            text.append(token)
            last_offset = end
        return ''.join(text)


"""
Extra problems to deals with
  - match versus actual token spans (choose the right policy, full token match, partial token match, etc.)
  - resolve conflicts when matches from different patterns overlap (assign a priority? pick the longest?)
            - we are anonymizing, picking the longest, and thus promoting recall over precision makes sense
  - 
"""

"""
A typical sequence-labelling data loader, for multitask (two outcomes) and with sliding windows, this with seq_len and ctx_len.
For training, the vocabularies are directly injected from an entities hierarchy for level1/level2 tags (no longer inferred from the training data).
It can be leveraged at inference time also from a list of entries apart from input files.
"""
import json
from typing import List, Dict, ClassVar, Optional

import torch
from torch.utils.data import Dataset
from transformers import PreTrainedTokenizer

from mapa_project.data_processing.mapa_hierarchy import MapaEntitiesHierarchy
from mapa_project.entity_detection.common.field_definition import FieldDefinition
from mapa_project.entity_detection.common.misc_logic import TextCleaner
from mapa_project.entity_detection.common.pretokenization_helpers import TokenizationRemappingHelper, GeneralPreTokenizer
from mapa_project.entity_detection.common.windowed_sequences_helper import WindowedSequencesGenerator


class TwoFlatLevelsDataset(Dataset):
    O_TAG: ClassVar[str] = 'O'

    def __init__(self, instances: List[Dict[str, List[str]]], tokenizer: PreTrainedTokenizer,
                 valid_seq_len: int,
                 ctx_len: int,
                 input_field: FieldDefinition,
                 level1_tags_field: FieldDefinition,
                 level2_tags_field: FieldDefinition,
                 build_vocabs: bool,
                 entities_hierarchy: Optional[MapaEntitiesHierarchy],
                 train_subwords: bool = True,
                 loss_fct_ignore_index: int = -100,
                 linebreak_token: str = None
                 ):
        # Fail-fast check, twice the ctx_len plus the valid seq_len cannot be greater than max_len (512 - 2 for BERT)
        # Note that we are hardcoding the max_len here, which may differ for other Transformer-based model
        absolute_max_len = 512 - 2  # minus two to account for the CLS and SEP tokens
        if (2 * ctx_len + valid_seq_len) > absolute_max_len:
            raise Exception(f'Valid sequence len ({valid_seq_len}) and ctx_len ({ctx_len}) exceed the absolute max len of {absolute_max_len}')

        self.original_instances = instances

        ################
        # Text cleaning added (to replace weird characters that cause [UNK] tokens with BERT)
        self.__clean_tokens_(instances=self.original_instances, tokens_field_name=input_field.name, text_cleaner=TextCleaner())
        ################

        self.tokenizer = tokenizer
        if linebreak_token:
            self.__add_linebreak_token_to_tokenizer_(self.tokenizer, linebreak_token=linebreak_token)
        self.valid_seq_len = valid_seq_len
        self.ctx_len = ctx_len
        self.input_field = input_field
        self.level1_tags_field = level1_tags_field
        self.level2_tags_field = level2_tags_field

        # To choose whether model subwords as 'X' tags or do not train them (omit backpropagation signal on them)
        self.train_subwords = train_subwords
        # the loss function ignore index (for CrossEntropyLoss on Pytorch it is -100)
        self.loss_fct_ignore_index = loss_fct_ignore_index

        self.tokens_remapper = TokenizationRemappingHelper(self.original_instances, tokenizer=self.tokenizer, tokens_field_name=input_field.name,
                                                           tag_fields_names=(level1_tags_field.name, level2_tags_field.name,))

        if build_vocabs:
            level1_labels: List[str] = entities_hierarchy.get_level1_labels(bio=True, lowercase=False)
            level2_labels: List[str] = entities_hierarchy.get_all_level2_labels(bio=True, lowercase=False)
            if self.train_subwords:
                level1_labels = [self.tokens_remapper.subword_tag] + level1_labels
                level2_labels = [self.tokens_remapper.subword_tag] + level2_labels
            self.level1_tags_field.build_labels_vocab(direct_vocab=level1_labels)
            self.level2_tags_field.build_labels_vocab(direct_vocab=level2_labels)

        windowed_sequences_generator = WindowedSequencesGenerator(valid_seq_len=valid_seq_len, ctx_len=ctx_len,
                                                                  fields_to_process=[self.input_field,
                                                                                     self.level1_tags_field, self.level2_tags_field],
                                                                  add_cls_sep=True)
        self.windowed_instances = windowed_sequences_generator.transform_sequences_to_windows_with_ctx(self.tokens_remapper.retokenized_instances)

    @classmethod
    def __add_linebreak_token_to_tokenizer_(cls, tokenizer: PreTrainedTokenizer, linebreak_token: str):
        tokenizer.add_tokens([linebreak_token])

    @classmethod
    def __clean_tokens_(cls, instances: List[Dict[str, List[str]]], tokens_field_name, text_cleaner: TextCleaner):
        for instance in instances:
            # convert into a list so items can be reassigned (sometimes it becomes a tuple... need to find where...)
            instance[tokens_field_name] = list(instance[tokens_field_name])
            for i, token in enumerate(instance[tokens_field_name]):
                instance[tokens_field_name][i] = text_cleaner.clean_text(token)

    @classmethod
    def load_from_file(cls, input_path, tokenizer: PreTrainedTokenizer,
                       valid_seq_len: int,
                       ctx_len: int,
                       input_field: FieldDefinition,
                       level1_tags_field: FieldDefinition,
                       level2_tags_field: FieldDefinition,
                       build_vocabs: bool, entities_hierarchy: Optional[MapaEntitiesHierarchy], train_subwords: bool):
        with open(input_path, 'r', encoding='utf-8') as f:
            instances = [json.loads(line) for line in f.readlines()]
        return TwoFlatLevelsDataset(instances, tokenizer, valid_seq_len, ctx_len, input_field, level1_tags_field, level2_tags_field, build_vocabs,
                                    entities_hierarchy, train_subwords=train_subwords)

    @classmethod
    def load_for_inference(cls, pretokenized_input: List[List[str]], tokenizer: PreTrainedTokenizer,
                           valid_seq_len: int,
                           ctx_len: int,
                           input_field: FieldDefinition,
                           level1_tags_field: FieldDefinition,
                           level2_tags_field: FieldDefinition):
        # build synthetic instances (with dummy O labels) using the pretokenized input, to be compliant with the workflow of the dataset
        instances = [{input_field.name: tokens, level1_tags_field.name: ['O'] * len(tokens), level2_tags_field.name: ['O'] * len(tokens)} for tokens
                     in pretokenized_input]
        return TwoFlatLevelsDataset(instances, tokenizer, valid_seq_len, ctx_len, input_field, level1_tags_field, level2_tags_field,
                                    build_vocabs=False,
                                    entities_hierarchy=None)

    def __len__(self):
        return len(self.windowed_instances)

    def __getitem__(self, item):
        return self.__process_instance(self.windowed_instances[item])

    def __process_instance(self, instance):
        instance_num = instance[WindowedSequencesGenerator.INSTANCE_IDX]
        instance_order = instance[WindowedSequencesGenerator.INSTANCE_ORDER]

        tokens = instance[self.input_field.name]
        level1_tags = instance[self.level1_tags_field.name]
        level2_tags = instance[self.level2_tags_field.name]

        input_ids = torch.tensor(self.tokenizer.convert_tokens_to_ids(tokens))
        attention_mask = torch.ne(input_ids, self.tokenizer.pad_token_id).long()
        # note the +1 to the ctx_len to include the special tokens (cls/sep) as part of the "ignored" context
        ctx_mask = torch.tensor([0] * (self.ctx_len + 1) + [1] * self.valid_seq_len + [0] * (self.ctx_len + 1))
        if self.train_subwords:
            level1_tags_ids = torch.tensor([self.level1_tags_field.stoi(label) for label in level1_tags])
            level2_tags_ids = torch.tensor([self.level2_tags_field.stoi(label) for label in level2_tags])
        else:
            level1_tags_ids = torch.tensor([self.level1_tags_field.stoi(label)
                                            if label != self.tokens_remapper.subword_tag else self.loss_fct_ignore_index for label in level1_tags])
            level2_tags_ids = torch.tensor([self.level2_tags_field.stoi(label)
                                            if label != self.tokens_remapper.subword_tag else self.loss_fct_ignore_index for label in level2_tags])

        processed_instance = {
            WindowedSequencesGenerator.INSTANCE_IDX: torch.tensor(instance_num, dtype=torch.long),
            WindowedSequencesGenerator.INSTANCE_ORDER: torch.tensor(instance_order, dtype=torch.long),
            self.input_field.name: input_ids,
            self.input_field.attn_mask_name(): attention_mask,
            self.input_field.name + '_ctx_mask': ctx_mask,
            self.level1_tags_field.name: level1_tags_ids,
            self.level2_tags_field.name: level2_tags_ids
        }
        return processed_instance

# if __name__ == '__main__':
#     # some quick testing... to see if the data processing is correct...
#
#     # inputs = [
#     #     "F'dawk il-każijiet l-applikanti applikaw għal pensjoni fl-Awstrija wara l-isħubija u talbu li ċerti fatti li seħħew fi Stati Membri oħra qabel l-isħubija jittieħdu in konsiderazzjoni (li fil-każ Kauer kienu jikkonċernaw il-perjodu ta' trobbija ta' tifel fil-Belġju u, fil-każ Duchon, inċident li seħħ il-Ġermanja u li rrriżulta f'mankament)."]
#     #
#     # pretokenizer = GeneralPreTokenizer(prepend_whitespaces=False, remove_linebreaks=True)
#     # pretokenized_input = [pretokenizer.do_pretokenization(raw_text=text, calculate_offsets=False) for text in inputs]
#     tokenizer = BertTokenizer.from_pretrained('/DATA/agarciap_data/MAPA_STUFF/MAPA_EXPERIMENTS/bert-base-multilingual-cased_vocab_extended/',
#                                               do_lower_case=False)
#     input_field = FieldDefinition.create_sequential_field('tokens', multilabel=False, tokenizer=tokenizer)
#     level1_tags_field = FieldDefinition.create_sequential_field('level1_tags', multilabel=False, tokenizer=tokenizer)
#     level2_tags_field = FieldDefinition.create_sequential_field('level2_tags', multilabel=False, tokenizer=tokenizer)
#
#     # input_path = '/DATA/agarciap_data/MAPA_STUFF/MAPA_EXPERIMENTS/CONVERTED_DATA/UMALTA_MT/UMALTA_MT_TRAIN.json'
#     lang = 'ES'
#     input_path = f'/DATA/agarciap_data/MAPA_STUFF/MAPA_EXPERIMENTS/CONVERTED_DATA2/EURLEX/{lang}/{lang}_DEV.json'
#
#     dataset = TwoFlatLevelsDataset.load_from_file(input_path=input_path,
#                                                   tokenizer=tokenizer,
#                                                   valid_seq_len=300,
#                                                   ctx_len=100,
#                                                   input_field=input_field,
#                                                   level1_tags_field=level1_tags_field,
#                                                   level2_tags_field=level2_tags_field,
#                                                   build_vocabs=True, entities_hierarchy=MAPA_ENTITIES_HIERARCHY,
#                                                   train_subwords=True)
#
#     print()
#     windowed_instance = dataset.windowed_instances[0]
#     tokens = windowed_instance[input_field.name]
#     l1_tags = windowed_instance[level1_tags_field.name]
#     l2_tags = windowed_instance[level2_tags_field.name]
#
#     for i, token in enumerate(tokens):
#         print(f'{token} / {l1_tags[i]} / {l2_tags[i]}')
#
#     print('###################')
#
#     print(dataset[0])
#
#     print(level1_tags_field.vocab)
#     print(level1_tags_field.special_tokens)
#     print(level2_tags_field.vocab)

import json
from itertools import chain
from typing import List

from sklearn.metrics import precision_score, f1_score, recall_score

from mapa_project.entity_detection.evaluation.conll_evaluator import evaluate
from mapa_project.entity_detection.inference.entities_detection_inferencer import MAPATwoFlatLevelsInferencer, MapaSeqLabellingOutput


class MapaEntityDetectorEvaluator:

    def __init__(self, inferencer: MAPATwoFlatLevelsInferencer):
        # later it would be good to have a more flexible thing here to allow different inferencer variations...
        self.inferencer = inferencer

    def evaluate(self, test_data_path: str):
        with open(test_data_path, 'r', encoding='utf-8') as f:
            instances = [json.loads(line) for line in f.readlines()]

        all_tokens: List[List[str]] = [instance['tokens'] for instance in instances]
        all_level1_gold_tags: List[List[str]] = [instance['level1_tags'] for instance in instances]
        all_level2_gold_tags: List[List[str]] = [instance['level2_tags'] for instance in instances]

        outputs: List[MapaSeqLabellingOutput] = self.inferencer.make_inference(pretokenized_input=all_tokens)

        all_level1_pred_tags: List[List[str]] = [output.level1_tags for output in outputs]
        all_level2_pred_tags: List[List[str]] = [output.level2_tags for output in outputs]

        chained_level1_gold_tags = list(chain(*all_level1_gold_tags))
        chained_level2_gold_tags = list(chain(*all_level2_gold_tags))
        chained_level1_pred_tags = list(chain(*all_level1_pred_tags))
        chained_level2_pred_tags = list(chain(*all_level2_pred_tags))

        level1_labels_to_eval = list(set(chained_level1_gold_tags) - {'O'})
        level1_miP = precision_score(chained_level1_gold_tags, chained_level1_pred_tags, labels=level1_labels_to_eval, average='micro')
        level1_miR = recall_score(chained_level1_gold_tags, chained_level1_pred_tags, labels=level1_labels_to_eval, average='micro')
        level1_miF = f1_score(chained_level1_gold_tags, chained_level1_pred_tags, labels=level1_labels_to_eval, average='micro')
        ########
        level2_labels_to_eval = list(set(chained_level2_gold_tags) - {'O'})
        level2_miP = precision_score(chained_level2_gold_tags, chained_level2_pred_tags, labels=level2_labels_to_eval, average='micro')
        level2_miR = recall_score(chained_level2_gold_tags, chained_level2_pred_tags, labels=level2_labels_to_eval, average='micro')
        level2_miF = f1_score(chained_level2_gold_tags, chained_level2_pred_tags, labels=level2_labels_to_eval, average='micro')

        print('-------------------\nMAPA LEVEL 1 TAGS TOKEN-WISE evaluation:\n-------------------')
        print(f'miP: {level1_miP:.3f}   miR: {level1_miR:.3f}   miF: {level1_miF:.3f}')
        print('-------------------\nMAPA LEVEL 1 TAGS CoNLL-WISE evaluation:\n-------------------')
        evaluate(chained_level1_gold_tags, chained_level1_pred_tags, verbose=True)
        print('-------------------\nMAPA LEVEL 2 TAGS TOKEN-WISE evaluation:\n-------------------')
        print(f'miP: {level2_miP:.3f}   miR: {level2_miR:.3f}   miF: {level2_miF:.3f}')
        print('-------------------\nMAPA LEVEL 2 TAGS CoNLL-WISE evaluation:\n-------------------')
        evaluate(chained_level2_gold_tags, chained_level2_pred_tags, verbose=True)

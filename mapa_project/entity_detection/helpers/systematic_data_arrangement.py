"""
A helper to do the following:
 - Obtain and copy the train/dev/test split into their corresponding folders from the full_dataset
 - Transform the tsv files into jsonlines suitable for MAPA and copy them into mirrored folder in a parallel folder structure

It assumes a layout as:
 - ANNOTATED_DATA
     - LANG
         - DOMAIN
            - annotated
               - full_dataset
                  - xxxx1.tsv
               - train
               - dev
               - test
            - predictions
            - source

A mirrored structure (for the converted json files) would be the same but only having the train/dev/set folders.
We need sophisticated data structures to configure this all properly instead of passing tuples and dicts around...
"""
# import json
import logging
import os.path
import shutil
from dataclasses import dataclass
from typing import List, Tuple

from sklearn.model_selection import train_test_split

from mapa_project import init_logger
from mapa_project.data_processing.jsonlines_files_generation import bulk_convert_tsv_to_jsonlines, convert_single_tsv_file_to_jsonlines

init_logger()
logger = logging.getLogger(__name__)


@dataclass
class LangDataset:
    lang: str
    datasets: List[str]


class DataArrangementHelper:

    def __init__(self, root_folder: str, lang_datasets: List[LangDataset],
                 train_dev_test_ratios: Tuple[float, float, float], random_seed: int = 42):
        self.root_folder = root_folder
        self.lang_datasets = lang_datasets
        self.train_ratio, self.dev_ratio, self.test_ratio = train_dev_test_ratios
        self.random_seed = random_seed

    def split_language_files(self):
        logger.info(f'Starting process for: {self.lang_datasets}')
        for lang_datasets in self.lang_datasets:
            lang = lang_datasets.lang
            datasets = lang_datasets.datasets
            logger.info(f'Processing lang: {lang}')
            lang_folder = os.path.join(self.root_folder, lang)
            # domain_folders = [os.path.join(lang_folder, file) for file in os.listdir(lang_folder) if os.path.isdir(os.path.join(lang_folder, file))]
            # logger.info(f'Domain folders found: {domain_folders}')
            logger.info(f'Datasets to process: {datasets}')
            for dataset in datasets:
                dataset_folder = os.path.join(lang_folder, dataset)
                logger.info(f'Processing dataset folder: {dataset_folder}')
                annotated_data_folder = os.path.join(dataset_folder, 'annotated')
                self.__split_into_train_dev_test(annotated_data_folder)

    def __split_into_train_dev_test(self, annotated_data_folder: str):
        full_dataset_folder = os.path.join(annotated_data_folder, 'full_dataset')
        if not os.path.exists(full_dataset_folder):
            logger.warning(f'Folder {full_dataset_folder} DOES NOT exist, omitting it...')
            return
        file_paths = [os.path.join(full_dataset_folder, file) for file in os.listdir(full_dataset_folder)
                      if os.path.isfile(os.path.join(full_dataset_folder, file))]
        train_files, dev_n_test_files = train_test_split(file_paths, train_size=self.train_ratio, random_state=self.random_seed)
        dev_files, test_files = train_test_split(dev_n_test_files, train_size=self.dev_ratio / (self.dev_ratio + self.test_ratio),
                                                 random_state=self.random_seed)
        logger.info(f'Total files: {len(file_paths)} --> Train: {len(train_files)}  Dev: {len(dev_files)}  Test: {len(test_files)}')
        assert len(dev_files) >= 1, 'Dev files ended up empty? No way... too few total files...?'

        self.__copy_to_folder(train_files, os.path.join(annotated_data_folder, 'train'))
        self.__copy_to_folder(dev_files, os.path.join(annotated_data_folder, 'dev'))
        self.__copy_to_folder(test_files, os.path.join(annotated_data_folder, 'test'))

    @classmethod
    def __copy_to_folder(cls, files: List[str], destination: str):
        for file in files:
            logger.info(f'Copying {file} to {destination}')
            shutil.copy(file, os.path.join(destination, os.path.basename(file)))

    def convert_files(self, mirror_root_path: str):
        for lang_datasets in self.lang_datasets:
            lang = lang_datasets.lang
            datasets = lang_datasets.datasets
            logger.info(f'Processing lang: {lang}')
            lang_folder = os.path.join(self.root_folder, lang)
            # domain_folders = [os.path.join(lang_folder, file) for file in os.listdir(lang_folder) if os.path.isdir(os.path.join(lang_folder, file))]
            # logger.info(f'Domain folders found: {domain_folders}')
            logger.info(f'Datasets to process: {datasets}')
            for dataset in datasets:
                dataset_folder = os.path.join(lang_folder, dataset)
                logger.info(f'Processing dataset folder: {dataset_folder}')
                annotated_data_folder = os.path.join(dataset_folder, 'annotated')
                if not os.path.exists(annotated_data_folder):
                    logger.warning(f'No data for {annotated_data_folder}, skipping...')
                    continue
                self.__convert_all_tsv_files_into_single_jsonlines(annotated_data_folder, 'train', mirror_root_path=mirror_root_path)
                self.__convert_all_tsv_files_into_single_jsonlines(annotated_data_folder, 'dev', mirror_root_path=mirror_root_path)
                # NOTE that test files are converted individually, so later the predictions can be generated file by file
                self.__convert_tsv_files_into_individual_jsonlines(annotated_data_folder, 'test', mirror_root_path=mirror_root_path)
                # train_files = self.__get_tsv_files(annotated_data_folder, 'train')
                # self.__convert_all_tsv_files(train_files, mirror_root_path=mirror_root_folder)
                # dev_files = self.__get_tsv_files(annotated_data_folder, 'dev')
                # self.__convert_all_tsv_files(dev_files, mirror_root_path=mirror_root_folder)
                # test_files = self.__get_tsv_files(annotated_data_folder, 'test')
                # self.__convert_all_tsv_files(test_files, mirror_root_path=mirror_root_folder)

    @classmethod
    def __get_tsv_files(cls, base_path: str, folder: str):
        folder_path = os.path.join(base_path, folder)
        return [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.tsv')]

    def __convert_all_tsv_files_into_single_jsonlines(self, base_folder: str, partition_name: str, mirror_root_path: str):
        rel_path = os.path.relpath(base_folder, self.root_folder)
        destination_path = os.path.join(mirror_root_path, os.path.join(rel_path, partition_name + '.jsonl'))
        os.makedirs(os.path.dirname(destination_path), exist_ok=True)
        bulk_convert_tsv_to_jsonlines(input_folders=[os.path.join(base_folder, partition_name)], output_path=destination_path, retokenize_tokens=True)

    def __convert_tsv_files_into_individual_jsonlines(self, base_folder: str, partition_name: str, mirror_root_path: str):
        rel_path = os.path.relpath(base_folder, self.root_folder)
        # here we have the base destination folder (e.g. xxx/test/ ) in which the individual files will be generated
        base_destination_path = os.path.join(mirror_root_path, os.path.join(rel_path, partition_name))
        os.makedirs(base_destination_path, exist_ok=True)
        partition_source_folder = os.path.join(base_folder, partition_name)
        tsv_file_paths = [os.path.join(partition_source_folder, file) for file in os.listdir(partition_source_folder) if file.endswith('.tsv')]
        for tsv_file_path in tsv_file_paths:
            convert_single_tsv_file_to_jsonlines(tsv_file_path=tsv_file_path, base_output_path=base_destination_path, retokenize_tokens=True)

    # def __convert_and_copy(self, tsv_file_path: str, mirror_root_path: str):
    #     rel_path = os.path.relpath(tsv_file_path, self.root_folder)
    #     destination_path = os.path.join(mirror_root_path, rel_path.replace('.tsv', '.jsonl'))
    #     os.makedirs(os.path.dirname(destination_path), exist_ok=True)
    #     parsed_document = parse_inception_file(input_path=tsv_file_path)
    #     instance = convert_parsed_document_to_sequence_labelling(parsed_document=parsed_document, retokenize_tokens=True)
    #     logger.info(f'Writing converted {tsv_file_path} to {destination_path}')
    #     with open(destination_path, 'w', encoding='utf-8') as f:
    #         f.writelines([json.dumps(instance) + '\n'])


if __name__ == '__main__':
    # quick check of the process
    # init_logger()

    root_folder = '/DATA/agarciap_data/MAPA_STUFF/MAPA_FINAL/systematic_trainings/trainings_for_eval/ANNOTATED_DATA'
    mirror_root_folder = '/DATA/agarciap_data/MAPA_STUFF/MAPA_FINAL/systematic_trainings/trainings_for_eval/CONVERTED_ANNOTATED_DATA/'
    lang_datasets = [
        LangDataset('EN', ['LEGAL', 'MEDICAL', 'EUR_LEX', 'ADMINISTRATIVE-LEGAL']),
        LangDataset('FR', ['LEGAL/COUR_CASSATION1', 'MEDICAL/CAS1', 'EUR_LEX']),
        LangDataset('ES', ['LEGAL/Procesos_Legales',
                           'MEDICAL/MEDDOCAN_Gold',  # MEDDOCAN is already split into official partitions
                           'EUR_LEX']),
        LangDataset('EL', ['LEGAL/AREIOSPAGOS1', 'EUR_LEX']),
        LangDataset('MT', ['LEGAL/Jurisprudence_1', 'MEDICAL', 'EUR_LEX', 'ADMINISTRATIVE', 'GENERAL_NEWS/News_1']),
    ]
    train_dev_test_ratios = (0.7, 0.1, 0.2)
    data_arrangement_helper = DataArrangementHelper(root_folder=root_folder, lang_datasets=lang_datasets, train_dev_test_ratios=train_dev_test_ratios,
                                                    random_seed=42)

    # data_arrangement_helper.split_language_files()
    data_arrangement_helper.convert_files(mirror_root_path=mirror_root_folder)

"""
An entity replacer based on gazetteers (name dictionaries and lists)
"""
import logging
import os.path
import random
from dataclasses import dataclass
from typing import Dict, List, Set

import confuse
from confuse import Configuration

from mapa_project.anonymization.datatypes_definition import Tokens, Labels
from mapa_project.anonymization.helpers import remove_bio_tagging

logger = logging.getLogger(__name__)


@dataclass
class GazetteerReplacerConfig:
    enabled: bool
    particles_to_omit: Set[str]
    default_config: Dict[str, Set[str]]
    language_based_configs: Dict[str, Dict[str, Set[str]]]

    @classmethod
    def load_from_config(cls, base_folder: str, config: Configuration) -> 'GazetteerReplacerConfig':
        gazetteer_config = config['replacement']['gazetteer_replacer']
        enabled = gazetteer_config['enabled'].get(bool)
        logger.info(f'Loading gazetteer configuration (enabled:{enabled})....')
        particles_to_omit = set(gazetteer_config['particles_to_omit'].get(list))
        languages_config = gazetteer_config['language_config'].get(confuse.Optional(dict, default={}))
        loaded_language_based_configs: Dict[str, Dict[str, Set[str]]] = {}
        for lang, entities_config in languages_config.items():
            if lang not in loaded_language_based_configs:
                loaded_language_based_configs[lang] = {}
            for entity, gazetteer_file in entities_config.items():
                loaded_language_based_configs[lang][entity] = cls.__load_gazetteer(base_folder, gazetteer_file)

        default_config = gazetteer_config['default_config'].get(confuse.Optional(dict, default={}))
        default_loaded_config: Dict[str, Set[str]] = {key: cls.__load_gazetteer(base_folder, value) for key, value in default_config.items()}
        return GazetteerReplacerConfig(enabled=enabled, particles_to_omit=particles_to_omit,
                                       default_config=default_loaded_config, language_based_configs=loaded_language_based_configs)

    @classmethod
    def __load_gazetteer(cls, base_folder: str, file_name: str) -> Set[str]:
        file_path = os.path.join(base_folder, file_name)
        with open(file_path, 'r', encoding='utf-8') as f:
            lines = f.readlines()
        names: List[str] = []
        for line in lines:
            line = line.strip()
            if line == '' or line.startswith('#'):
                continue
            names.append(line)
        return set(names)


class GazetteerReplacer:

    def __init__(self, config: GazetteerReplacerConfig):
        self.config = config

    def update_operational_config(self, config: Configuration, base_folder: str):
        """ A method that further propagates (or applies) the configuration changes that can be updated on-the-fly """
        self.config = GazetteerReplacerConfig.load_from_config(config=config, base_folder=base_folder)

    def replace_entities(self, tokens: Tokens, level1_labels: Labels, level2_labels: Labels, lang: str) -> Tokens:
        if not self.config.enabled:
            logger.info('Gazetteer based replacement is disabled')
            return tokens
        level1_labels_wo_bio = remove_bio_tagging(level1_labels)
        level2_labels_wo_bio = remove_bio_tagging(level2_labels)
        resulting_tokens: List[str] = tokens.copy()
        lang_based_config = self.config.language_based_configs.get(lang, self.config.default_config)
        replacements_memory: Dict[str, str] = {}
        for i, token in enumerate(tokens):
            level1_label = level1_labels_wo_bio[i]
            level2_label = level2_labels_wo_bio[i]
            if level2_label in lang_based_config or level1_label in lang_based_config:
                replacement = token
                if token in self.config.particles_to_omit:
                    replacement = ''
                elif token and token[0].isupper() and level2_label in lang_based_config:
                    replacement = replacements_memory.get(token, random.sample(lang_based_config[level2_label], k=1)[0])
                elif token and token[0].isupper() and level1_label in lang_based_config:
                    replacement = replacements_memory.get(token, random.sample(lang_based_config[level1_label], k=1)[0])
                replacements_memory[token] = replacement
                resulting_tokens[i] = replacement
        return resulting_tokens

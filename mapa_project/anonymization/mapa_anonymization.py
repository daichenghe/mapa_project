import logging
import time
from typing import List, Dict

import langdetect
from confuse import Configuration

from mapa_project.anonymization.datatypes_definition import MapaResult, MapaEntity
from mapa_project.entity_detection.mapa_entity_detection import MapaEntityDetector, EntityDetectionResult
from mapa_project.entity_replacement.mapa_entity_obfuscation import MapaEntityObfuscator
from mapa_project.entity_replacement.mapa_entity_replacement import MapaEntityReplacer
from mapa_project.entity_detection.common.pretokenization_helpers import GeneralPreTokenizer, CharSpan
from mapa_project.webservice.app.web_api_formats import AnonymizationOp

logger = logging.getLogger(__name__)


class MapaAnonymizer:

    def __init__(self, entity_detector: MapaEntityDetector, entity_replacer: MapaEntityReplacer, entity_obfuscator: MapaEntityObfuscator):
        # TODO: watch out the pre-tokenizer configuration, in particular the linebreak stuff
        self.pretokenizer = GeneralPreTokenizer(prepend_whitespaces=False, remove_linebreaks=False, add_linebreak_tokens=False)
        self.entity_detector = entity_detector
        self.entity_obfuscator = entity_obfuscator
        self.entity_replacer = entity_replacer

    # NOTE: we are breaking the (anyway brittle) isolation from "confuse" Configuration, creating an ugly technical debt and too much coupling
    # but unless we redesign everything this is the only way to have a hot-reloading mechanism to update certain operational parameters but not all
    # The design flaw (among others) is that the config is handled by the "XXXConfig" class in each case, but it is only for full initialization
    # We want to update already created objects. The config reading/application is very diverse and confusing...
    def update_operational_config(self, config: Configuration, base_folder: str):
        """ A method that further propagates (or applies) the configuration changes that can be updated on-the-fly """
        self.entity_detector.update_operational_config(config=config, base_folder=base_folder)
        self.entity_obfuscator.update_operational_config(config=config)
        self.entity_replacer.update_operational_config(config=config, base_folder=base_folder)

    def anonymize(self, text, anonymization_op: AnonymizationOp) -> MapaResult:
        start_time = time.time()
        lang = langdetect.detect(text)
        logger.info(f'Text language detected as >> {lang} << (it may have impact on some processes)')
        # just in case, remove \r character
        text = text.replace('\r\n', '\n').replace('\r', '\n')
        tokens, spans = zip(*self.pretokenizer.do_pretokenization(raw_text=text.strip(), calculate_offsets=True))
        linebreaks_positions_and_counts = self.__obtain_tokens_numbers_before_line_breaks(tokens)
        # now that the linebreaks are accounted, remove them to clean the tokens (and their span list)
        tokens, spans = zip(*[(token, spans[i]) for i, token in enumerate(tokens) if token != '\n'])
        detection_result: EntityDetectionResult = self.entity_detector.detect_entities(text=text, tokens=tokens, spans=spans)
        level1_tags, level2_tags = detection_result.level1_tags, detection_result.level2_tags
        if anonymization_op == AnonymizationOp.REPLACE:
            reprocessed_text, reprocessed_spans = self.entity_replacer.replace_entities(
                tokens=tokens, spans=spans, linebreaks_positions_and_counts=linebreaks_positions_and_counts,
                level1_labels=level1_tags, level2_labels=level2_tags, lang=lang)
        elif anonymization_op == AnonymizationOp.OBFUSCATE:
            reprocessed_text = self.entity_obfuscator.obfuscate_text(text=text, tokens=tokens, spans=spans,
                                                                     level1_labels=level1_tags, level2_labels=level2_tags)
            reprocessed_spans = spans
        elif anonymization_op == AnonymizationOp.TYPE_SUBSTITUTION:
            raise NotImplementedError('Type substitution not implemented in this version (it does not provide much)')
        else:
            logger.info('Doing no entity_replacement nor obfuscation. Content will be returned without any [pseudo]anonymization.')
            reprocessed_text = text
            reprocessed_spans = spans

        level1_entities: List[MapaEntity] = self.__obtain_entities_from_bio_tagging(reprocessed_text, reprocessed_spans, level1_tags)
        level2_entities: List[MapaEntity] = self.__obtain_entities_from_bio_tagging(reprocessed_text, reprocessed_spans, level2_tags)

        end_time = time.time()
        analysis_time = end_time - start_time
        using_gpu = 'cuda' in self.entity_detector.nerc_inferencer.device
        performing_replacement = anonymization_op == AnonymizationOp.REPLACE
        mapa_result = MapaResult(text=reprocessed_text, level1_entities=level1_entities, level2_entities=level2_entities,
                                 num_tokens=len(tokens), analysis_seconds=analysis_time, using_gpu=using_gpu,
                                 performing_replacement=performing_replacement)
        return mapa_result

    @classmethod
    def __obtain_tokens_numbers_before_line_breaks(cls, tokens):
        """ Obtain token positions that precede one or more line break (accounting the number). NOTE: positions are shifted ignoring line-breaks """
        # logger.info('TOKENS to obtain linebreaks', tokens)
        linebreaks_positions_and_counts: Dict[int, int] = {}
        last_non_linebreak_pos = -1
        num_line_breaks_so_far = 0
        for i, token in enumerate(tokens):
            if token != '\n':  # regular token, update last valid position
                last_non_linebreak_pos = i - num_line_breaks_so_far
            elif last_non_linebreak_pos == -1:  # line break before any valid token, ignore (but increase the count just in case)
                num_line_breaks_so_far += 1
            else:
                if last_non_linebreak_pos in linebreaks_positions_and_counts:
                    linebreaks_positions_and_counts[last_non_linebreak_pos] += 1
                else:
                    linebreaks_positions_and_counts[last_non_linebreak_pos] = 1
                num_line_breaks_so_far += 1

        # print('line break res', linebreaks_positions_and_counts)
        # print('>>>>>', [tokens[pos] for pos in linebreaks_positions_and_counts.keys()])
        return linebreaks_positions_and_counts

    @classmethod
    def __obtain_entities_from_bio_tagging(cls, text: str, spans: List[CharSpan], labels: List[str]) -> List[MapaEntity]:
        """ Given a BIO-tagged sequence return a list of type and spans"""
        mapa_entities: List[MapaEntity] = []
        current_type = ''
        current_span_start = -1
        current_span_end = -1
        for i, label in enumerate(labels):
            start, end = spans[i]
            if label.startswith('B-') or label == 'O':
                if current_type != '':
                    mapa_entities.append(MapaEntity(id=len(mapa_entities), entity_text=text[current_span_start:current_span_end],
                                                    offset=current_span_start, entity_type=current_type))
                    current_type = ''
                    current_span_start = -1
                    current_span_end = -1
                if label.startswith('B-'):
                    current_type = label.replace('B-', '')
                    current_span_start = start
                    current_span_end = end
            elif label.startswith('I-'):
                current_span_end = end

        # pick the last after loop, if any
        if current_type != '':
            mapa_entities.append(MapaEntity(id=len(mapa_entities), entity_text=text[current_span_start:current_span_end],
                                            offset=current_span_start, entity_type=current_type))

        return mapa_entities

from typing import List


def strip_bio_tagging(label: str):
    if '-' in label:
        return label.split('-', maxsplit=1)[1]
    else:
        return label


def remove_bio_tagging(bio_tagged_labels: List[str]):
    return [strip_bio_tagging(label) for label in bio_tagged_labels]

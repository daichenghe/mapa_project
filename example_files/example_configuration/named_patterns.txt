# The expected format is LABEL::REGEX
# One per line
# Empty lines or lines starting by '#' are ignored

DNI::\d{8}[A-Z]
PHONE::9\d{1,2}[\-\s]?(?:(?:\d{2})[\-\s]?){3}
IBAN::([a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16})
EMAIL::[\w\.]+@[\w\.]+\.[\w]+
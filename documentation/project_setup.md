## Usage of this code

This code has several capabilities, like model training, model inference, and also 
auxiliary methods like evaluation or data format conversion.
The source code is here for anybody to read it, but in order to simplify its usage, there
are some entrypoints that are invoked as regular commands.

NOTE: this should not be necessary to launch the service using Docker. Only to directly execute 
extra functionalities such as the entity detection model training.

First, create a Python virtual environment. It must be Python 3.7 or higher.
Activate your virtual environment and install Pytorch with CUDA capabilities (make sure that
you install the Pytorch version that matches the CUDA version in your system).

Now, clone this code, navigate to the root of the project (where the setup.py file is located) 
and run

```shell
python setup.py install
```

That should package and install the code in this repository as if it were a regular
python library, including its dependencies (except Pytorch, that you should have installed 
prior to this). Note that installation will happen inside the virtual environment you had active when the 
install command was run.

Now you can execute the provided functionalities.
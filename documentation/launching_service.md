# NOTE

**WARNING**: The way MAPA works (and thus how the demo is deployed) are changing from time to time due 
to additions and updates. This documentation may become incomplete or outdated at some point.

**DISCLAIMER**: There are tons of details to explain and document about the usage, and some of them
might be still missing. There might be also
some bugs or malfunctions (this is a work under development). Apologies in advance.
Do not hesitate contacting us if you experience any problem or have any doubt.

# Launching MAPA service and DEMO

**IMPORTANT:** *The internal inference logic has been updated! The previously shared model will not work
due to some minor differences on how the models are saved/loaded.
Use **[THIS](https://vicomtech.box.com/s/eno5i35ll2rv582roeizqp9jqzlcxpj5)** model instead, it 
should be compatible (it is the eurlex multilingual model, 
if you want some other model let us know or train them yourself).*


The code in this repository is basically a Python program.
The MAPA service and its associated demo/showcase frontend are deployed as web-services.

The easiest way to launch it, is using the Dockerfile and docker-compose files in this repository.
Just customize the environment variables (see the docker-compose), and the mounted volume, so the
provided MAPA models and config files can be read from the service.

Also, pay attention to which Pytorch cuda version you install (see the docker-compose and Dockerfile).
The Pytorch CUDA version must match the one in your system if you expect to use a GPU.
Otherwise, the models will be loaded in CPU. It is also an option, since the inference works 
reasonably fast even on CPU, but there is a 5x-10x speedup when using a GPU.

## Deployment instructions


There are some environment variables that are read by the application when it is launched:

  - **MAPA_BASE_FOLDER**: The path to the base folder for all the configuration assets. 
    It must include the 
    mapa_config.yaml file with all the configuration, the models and other files (such as 
    regular expressions files, name gazetteers, etc.). The files pointed in the mapa_config.yaml 
    are assumed to be inside this base folder.
  - **MAPA_PORT**: the port number on which the application will listen.

NOTE WHEN USING DOCKER:
*If you are not familiar to Docker you may get confused.
When using the provided docker-compose file, both the MAPA_BASE_FOLDER and the MAPA_PORT refer to the
Docker container. That means that MAPA_BASE_FOLDER will be a path internal to the container, not to
your host file system. In summary, when using the docker-compose file, you can leave the provided 
MAPA_BASE_FOLDER value (/MODELS), and simply mount your desired (host machine) folder into that 
internal path (the docker-compose.yml contains an example).
The same applies to the port. It will be the internal port used by the Docker container. Using the
docker-compose "ports" mapping, you can map any port from the host machine to the container.*

Check the information about [mapa_config.yaml](mapa_configuration.md) to see an example.
It is a rather large file, but the content is mostly self-explanatory (when it is not, it is probably 
an advanced option that does not need to be edited).

In the mapa_config.yaml you will need to refer to some other files.
For example, in order to use the regex module, you need to point to a regex file.
Such file has a very simple format (explained [here](regex_module.md)) and must live in the same base folder.

Once everything is ready, you should be able of executing the following command from the root of
the repository (where the docker-compose.yml file lives):
(provided you have Docker and docker-compose correctly installed)

```bash
# this will launch the building of the image (only need the first time, or if there are changes in the code
docker-compose build
```

Once the building process is finished (it may take a couple of minutes the very first time), them:

```bash
# this will launch the docker container with the application
# if you want to launch it as a background daemon process, you can use the -d flag
# however, the first time is better to launch in foreground to spot any problem that may arise
docker-compose up
```

If everything is correct (and taking into account how many configuration elements are involved, it is
unlikely that everything works at the first attempt), you will see a message saying that the application
is listening at a certain port (again, if you are using Docker, that port is the internal port to the
container, the actual port that will work for you is the one that you configured in the docker-compose.yml
file).

### The demo UI

The demo UI should appear deployed at:

http://**host**:**port**/mapa_beta/anonymization/model_showcase

Where **host** and **port** depend on how the application has been deployed.
When using Docker, *host* is the IP/DomainName of the host machine, and *port* is the external port configured in the
docker-compose.yml file.

Now the demo shows the official MAPA logo, together with the legend of the MAPA entity types.
There is the usual text-area to write or paste text.
Below it there is a dropdown to select which model stack to use among the configured ones.
By model stack we mean the combination of "detection model" and "replacement model".
The loaded models and the available combination stacks can be configured in the 
[mapa_config.yaml](mapa_configuration.md) file
before launching the application. It you change the configuration you need to restart the app for
the configuration to take effect.

There are also three radio buttons to select the operation:
  - only detect entities, but do not hide them whatsoever
  - detect entities and obfuscate (the configured ones) using asterisks
  - detect entities and perform a full replacement (following the configuration)

The last option (full replacement) takes a bit longer because it involves more complex processes.

### The web service

The web-service (it's documenting OpenAPI interface) should appear deployed at:

http://**host**:**port**/mapa_beta/docs

Where, again, **host** and **port** depend on how the application has been deployed.

The web-service keeps working just like before, returning a JSON answer, so it can be integrated
in another workflow or application. In fact, the demo UI just uses the same kind of information 
(entity types, spans and offsets) to plot the coloured labels in HTML.

The analysis service works like before, but it also admits a **model_label** parameter, which admits 
a model label just like the web UI. The **model_label** parameter is optional, if not present the
model deployed as the default will be used.

```json
{
  "text": "El señor Pérez.",
  "model_label": "eurlex_all+bert_multi",
  "operation": "OBFUSCATE"
}
```

The operation field admits three enumerated values: **OBFUSCATE**, **REPLACE** and **NOOP**.
The first one replaces the detected entities by asterisks, the second replaces the detected 
entities by another (hopefully) equivalent word, and the last one does nothing (returning the same 
input text). In all the cases the annotations that come together with the text are the same.

Example of curl call:

```bash
curl -X 'POST' \
  'https://snlt.vicomtech.org/mapa_beta/anonymization/anonymize_text' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "text": "El señor Pérez.",
  "model_label": "eurlex_all+bert_multi",
  "operation": "REPLACE"
}'
```

Output of **REPLACE** operation for the previous example, obtaining a text with an altered (replaced) entity:

```json
{
  "anonymized_text": "El señor Díaz.",
  "annotations": [
    {
      "content": "señor Díaz",
      "begin": 3,
      "end": 13,
      "value": "PERSON"
    },
    {
      "content": "señor",
      "begin": 3,
      "end": 8,
      "value": "title"
    },
    {
      "content": "Díaz",
      "begin": 9,
      "end": 13,
      "value": "family name"
    }
  ],
  "elapsed_seconds": 0.7942054271697998,
  "words_per_second": 3.7773602362435192,
  "used_device": "CPU"
}
```